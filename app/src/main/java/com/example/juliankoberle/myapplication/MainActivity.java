package com.example.juliankoberle.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnAdd;
    private Button btnSub;
    private Button btnMul;
    private Button btnDiv;
    private Button btnSolution;
    private Button btnClear;
    private TextView textOutput;
    private Button btnComma;

    private float solution;
    private float num1;
    private float num2;

    private String str = "";

    private List<Float> numbers;
    private List<Character> operators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btn0 = (Button)findViewById(R.id.btn0);
        this.btn1 = (Button)findViewById(R.id.btn1);
        this.btn2 = (Button)findViewById(R.id.btn2);
        this.btn3 = (Button)findViewById(R.id.btn3);
        this.btn4 = (Button)findViewById(R.id.btn4);
        this.btn5 = (Button)findViewById(R.id.btn5);
        this.btn6 = (Button)findViewById(R.id.btn6);
        this.btn7 = (Button)findViewById(R.id.btn7);
        this.btn8 = (Button)findViewById(R.id.btn8);
        this.btn9 = (Button)findViewById(R.id.btn9);
        this.btnAdd = (Button)findViewById(R.id.btnAdd);
        this.btnDiv = (Button)findViewById(R.id.btnDivide);
        this.btnMul= (Button)findViewById(R.id.btnMultiply);
        this.btnSolution = (Button)findViewById(R.id.btnSolution);
        this.btnClear = (Button) findViewById(R.id.btnClear);
        this.btnSub = (Button) findViewById(R.id.btnSub);
        this.textOutput = (TextView) findViewById(R.id.textOutput);
        this.btnComma = (Button) findViewById(R.id.btnComma);


        this.numbers = new LinkedList<>();
        this.operators = new LinkedList<>();



        btnClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                textOutput.setText("");
                str = "";
                solution = 0;
                numbers.clear();
                operators.clear();
            }
        });
        inputNumbers();
        //solving the calculation
        btnSolution.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                analyseString();
                solution = numbers.get(0);
                for (int i = 1 ; i < numbers.size() ; i++){

                    if (operators.get(i-1) == '-'){
                        solution  = solution - numbers.get(i);
                    }
                    if (operators.get(i-1) == '+'){
                        solution  = solution + numbers.get(i);
                    }
                    if (operators.get(i-1) == '/'){
                        solution  = solution / numbers.get(i);
                    }
                    if (operators.get(i-1) == '*'){
                        solution  = solution * numbers.get(i);
                    }
                }
                textOutput.setText(solution + "");
                str = solution + "";

                numbers.clear();
                operators.clear();
            }
        });
    }

    private void analyseString(){
        char[] charArray = str.toCharArray();
        String number = "";
        for (int i = 0; i < charArray.length ; i++){
            if (charArray[i] == '-'){
                operators.add('-');
                numbers.add(Float.parseFloat(number));
                number = "";
            }
            else if (charArray[i] == '+'){
                operators.add('+');
                numbers.add(Float.parseFloat(number));
                number = "";
            }
            else if (charArray[i] == '/'){
                operators.add('/');
                numbers.add(Float.parseFloat(number));
                number = "";
            }
            else if (charArray[i] == '*'){
                operators.add('*');
                numbers.add(Float.parseFloat(number));
                number = "";
            }
            else if ((Character.getNumericValue(charArray[i]) >= 0) && (Character.getNumericValue(charArray[i]) <= 9)){
                number += (Character.getNumericValue(charArray[i]));
            }
            else if (charArray[i] == '.'){
                number += '.';
            }
        }
        numbers.add(Float.parseFloat(number));
    }


    private void inputNumbers(){
        btn0.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "0";
                textOutput.setText(str);
            }
        });
        btn1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "1";
                textOutput.setText(str);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "2";
                textOutput.setText(str);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "3";
                textOutput.setText(str);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "4";
                textOutput.setText(str);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "5";
                textOutput.setText(str);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "6";
                textOutput.setText(str);
            }
        });
        btn7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "7";
                textOutput.setText(str);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "8";
                textOutput.setText(str);
            }
        });
        btn9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "9";
                textOutput.setText(str);
            }
        });
        btnMul.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "*";
                textOutput.setText(str);
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "/";
                textOutput.setText(str);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "+";
                textOutput.setText(str);
            }
        });
        btnSub.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += "-";
                textOutput.setText(str);
            }
        });
        btnComma.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                str += ".";
                textOutput.setText(str);
            }
        });


    }



}
